--Task 1. Theoretical: Explain the concept of a "Foreign Key" in SQL.
--Foreign Key is a column or combination of columns in table whose values match the values of a Primary Key column in another table. Using the Foreign Key, we can link two tables together. Foreign Key syntax like that:
--constraint order_details_order_id_FK foreign key (order_id) references orders (order_id)

--Task 2. Incomplete - Practical: Write a SQL query to fetch the details of the 'Sales' department.
select * from departments
where department_id = 80;


--Task 3. Problem-Solving: How would you find the employee with the highest salary who doesn't have a manager?
select first_name, max(salary) from employees
where manager_id is null
group by first_name;

--Task 4. Write a SQL query to fetch the name of the employee who earns the lowest salary in the company.
select first_name, min(salary) from employees
group by first_name
order by min(salary)
fetch next 1 rows only;

--Task 5. Incomplete - Practical: Write a SQL query to fetch the highest salary in each department.
select department_name, max(salary) as max_salary
from employees e
join departments d
on d.department_id = e.department_id
group by department_name;
